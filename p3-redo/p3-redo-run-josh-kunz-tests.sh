#!/usr/bin/env bash
# this script designed to work on Vulcan, not on CADE machines
#"$HOME/uofu-compilers-tests-master/proj3/test-default" "$HOME/p3-redo/pydesugar1"

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
testerdir="$DIR"/uofu-compilers-tests-master/proj3
#testdir="$DIR"/official-tests-without-disqualified-ones
testdir="$testerdir"/tests-original

echo "Running tests from Josh Kunz"
echo 
exec "$testerdir"/test-cases "$(which pylex)" "$(which pyparse)" "$(which sxpy)" "$DIR"/pydesugar1 "$testdir"

