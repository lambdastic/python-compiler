#!/usr/bin/env bash
# this script designed to work on Vulcan, not on CADE machines
#"$HOME/uofu-compilers-tests-master/proj3/test-default" "$HOME/p3-redo/pydesugar1"

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
testerdir="$DIR"/uofu-compilers-tests-master/proj3
testdir="$DIR"/official-tests-without-disqualified-ones

echo "Running official tests from directory $testdir"
echo "This directory contains symbolic links to tests located in /home/sarah/p3_test"
echo 
echo "ls -l $testdir"
ls -l "$testdir"
echo
echo "These tests exclude the following two disqualified tests:"
echo "    assignment_flattening_test2"
echo "    for_loop_elimination_more_complex_body_test"
echo
exec "$testerdir"/test-cases "$(which pylex)" "$(which pyparse)" "$(which sxpy)" "$DIR"/pydesugar1 "$testdir"

