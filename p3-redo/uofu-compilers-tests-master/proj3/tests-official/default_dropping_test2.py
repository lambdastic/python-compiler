def f():
    return 5

def func(a=10,*,b=f()):
    return a+b

print(func(1))
