#class with a class
class A:
    y = 10
    class B:
        z = 11

a = A()
b = a.B()

print(a.y)
print(b.z)


#class with a function
class X:
    def f(o, a):
        return a + 20

x = X()
print(x.f(3))
