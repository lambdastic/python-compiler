def wrapper(func):
    def inner(*args):
        print ("Wrapper does nothing")
        return func(*args)
    return inner

@wrapper
def func(a,b):
    x = 5
    return x + a + b

print(func(1,2))
