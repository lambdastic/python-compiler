Christopher S. Johnson 00428057
Heath J. French 00753645

Software dependencies: Racket (for compilation)

Everything appears to be working.  Hooray!

NOTE: it's best to run pylex from the same current working directory 
as it resides in; there was a bug (we believe it's patched) that caused it to
not find a dependency file and fail. This dependency is now compiled in its own
Racket source file and should be found without problem.

MANIFEST:

Makefile: for build
PropList.txt: source of information for identifiers (now hard-coded into 
    pylex.rkt)
UnicodeData.txt: contains information on Unicode characters
unicode-generate.rkt: responsible for extracting Unicode information from 
    UnicodeData.txt
attrs-charnames.rkt: GENERATED file produced by unicode-generate.rkt; 
    used by pylex.rkt
py-test/: directory with some test files
pylex.rkt: the main lexer program

