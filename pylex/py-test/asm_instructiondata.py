"""\
asm_instructiondata module -- part of the ASCRRAUT assembler package

Copyright (C) 2009 C. S. Johnson
    and The Rational Radicals group at The University of Utah
Licensed under the GNU General Public License, Version 3
    see http://www.gnu.org/licenses/gpl.html
"""


import sys
from abc import *  # abstract base
from pprint import pprint

from asm_foundation import *
from asm_instrfields import *


class HardwareInstrType(AbstractInstrType): # metaclass

    def __init__(cls, name, bases, dct, *,
                 mnemonics, operandTypes, instrFields):
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes)
        cls.instrFields = instrFields
        if instrFields:  # giant exception for MOVFI
            cls.mainOpcode = instrFields[0].encodeToBinary()

    def __repr__(cls):
        return """<HardwareInstr {!r}>""".format(cls.instrName)
        
 
class NOP_InstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        super().__init__(name, bases, dct,
                         mnemonics=("nop",),
                         operandTypes=(),
                         instrFields=(HardcodedField(16, 0x0020),))
        


class ExtOpcodeInstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct, *,
                 mnemonics, operandTypes, instrFields):
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)
        cls.extOpcode = instrFields[2].encodeToBinary()


class RegInstrType(ExtOpcodeInstrType): # metaclass

    def __init__(cls, name, bases, dct, *,
                 mnemonics, mainOpcode, extOpcode):
        instrFields = (HardcodedField(4, mainOpcode),
                       RegSpecifierField,
                       HardcodedField(4, extOpcode),
                       RegSpecifierField)
        operandTypes = (REG_OPRND_T, REG_OPRND_T)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)   


class Immed8bitInstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct, *,
                 mnemonics, mainOpcode):
        instrFields = (HardcodedField(4, mainOpcode),
                       RegSpecifierField,
                       Immediate8bitDataField)
        operandTypes = (IMMED_OPRND_T, REG_OPRND_T)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class Immed12bitInstrType(HardwareInstrType): # metaclass
    
    def __init__(cls, name, bases, dct, *,
                 mnemonics, mainOpcode):
        instrFields = (HardcodedField(4, mainOpcode),
                       RegSpecifierField,
                       Immediate12bitDataField)
        operandTypes = (IMMED_OPRND_T)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class ScondInstrType(ExtOpcodeInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        ##mnemonics = r"s(eq|ne|cs|cc|hi|ls|gt|le|fs|fc|lo|hs|lt|ge|uc)"
        mnemonics = ("s<cond>",)
        operandTypes = (REG_OPRND_T,)
        instrFields = (HardcodedField(4, 0b0100),
                       RegSpecifierField,
                       HardcodedField(4, 0b1101),
                       ConditionField)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class JcondInstrType(ExtOpcodeInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        ##mnemonics = r"j(eq|ne|cs|cc|hi|ls|gt|le|fs|fc|lo|hs|lt|ge|uc)"
        mnemonics = ("j<cond>",)
        operandTypes = (REG_OPRND_T,)
        instrFields = (HardcodedField(4, 0b0100),
                       ConditionField,
                       HardcodedField(4, 0b1100),
                       RegSpecifierField)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)



class BcondInstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        ##mnemonics=r"b(eq|ne|cs|cc|hi|ls|gt|le|fs|fc|lo|hs|lt|ge|uc)"
        mnemonics = ("b<cond>",)
        operandTypes = (PC_DISPMNT_OPRND_T,)
        instrFields = (HardcodedField(4, 0b1100),
                       ConditionField,
                       Immediate8bitDataField)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class BAL_InstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        ##mnemonics=r"b(eq|ne|cs|cc|hi|ls|gt|le|fs|fc|lo|hs|lt|ge|uc)"
        mnemonics = ("bal",)
        operandTypes = (REG_OPRND_T, PC_DISPMNT_OPRND_T)
        instrFields = (HardcodedField(8, 0b11001111),
                       Immediate8bitDataField)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


##class BcondLInstrType(ExtOpcodeInstrType): # metaclass
##    
##    def __init__(cls, name, bases, dct):
##        operandTypes = (REG_OPRND_T)
##        instrFields = (HardcodedField(0b1000, 4),
##                       ConditionField,
##                       HardcodedField(0b1100, 4),
##                       RegSpecifierField)
##        super().__init__(name, bases, dct,
##                         ##mnemonics="b(eq|ne|cs|cc|hi|ls|gt|" \
##                         ##         "le|fs|fc|lo|hs|lt|ge|uc)l",
##                         mnemonics=("b<cond>l",),
##                         operandTypes=operandTypes,
##                         instrFields=instrFields)


class LSHI_InstrType(HardwareInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        operandTypes = (SHIFT_AMNT_OPRND_T, REG_OPRND_T)
        instrFields = (HardcodedField(4, 0b1000),
                       RegSpecifierField,
                       HardcodedField(3, 0b000),
                       ShiftTypeField,
                       ShiftAmountField)
        super().__init__(name, bases, dct,
                         mnemonics=("lshi",),
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class LoadStoreInstrType(ExtOpcodeInstrType): # metaclass

    def __init__(cls, name, bases, dct, *,
                 mnemonics, operandTypes, mainOpcode, extOpcode):
        instrFields = (HardcodedField(4, mainOpcode),
                       RegSpecifierField,
                       HardcodedField(4, extOpcode),
                       RegSpecifierField)
        super().__init__(name, bases, dct,
                         mnemonics=mnemonics,
                         operandTypes=operandTypes,
                         instrFields=instrFields)


class LoadInstrType(LoadStoreInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        mainOpcode = 0b0100
        extOpcode = 0b0000
        operandTypes = (INDIRECT_REG_OPRND_T, REG_OPRND_T)
        super().__init__(name, bases, dct,
                         mnemonics=("load", "loadw"),
                         operandTypes=operandTypes,
                         mainOpcode=mainOpcode,
                         extOpcode=extOpcode)


class StorInstrType(LoadStoreInstrType): # metaclass

    def __init__(cls, name, bases, dct):
        mainOpcode = 0b0100
        extOpcode = 0b0100
        operandTypes = (REG_OPRND_T, INDIRECT_REG_OPRND_T)
        super().__init__(name, bases, dct,
                         mnemonics=("stor", "storw"),
                         operandTypes=operandTypes,
                         mainOpcode=mainOpcode,
                         extOpcode=extOpcode)


##class LoaddInstrType(ExtOpcodeInstrType): # metaclass
##
##    def __init__(cls, name, bases, dct):
##        instrFields = (HardcodedField(0b0000, 4),
##                       RegSpecifierField,
##                       HardcodedField(0b0100, 4),
##                       RegSpecifierField)
##        operandTypes = (INDIRECT_REG_OPRND_T, REG_OPRND_T)
##        super().__init__(name, bases, dct,
##                         mnemonics=("loadd",),
##                         operandTypes=operandTypes,
##                         instrFields=instrFields)
##
##        
##class StordInstrType(ExtOpcodeInstrType): # metaclass
##
##    def __init__(cls, name, bases, dct):
##        instrFields = (HardcodedField(0b0000, 4),
##                       RegSpecifierField,
##                       HardcodedField(0b1000, 4),
##                       RegSpecifierField)
##        operandTypes = (REG_OPRND_T, INDIRECT_REG_OPRND_T)
##        super().__init__(name, bases, dct,
##                         mnemonics=("stord",),
##                         operandTypes=operandTypes,
##                         instrFields=instrFields)

#### PSEUDO-INSTRUCTION TYPES ###








# regular (non-meta-) classes


MAX_INSTR_VALUE = (2 << 16) - 1

class HardwareInstr(AbstractInstr, EncodableOutputItem): # abstract class

    @abstractmethod
    def __init__(self, inv_mnemonic, operands, filepath, sourceline,
                 offset, labels):
        sizeinbits = self._instructionsize
        OutputItem.__init__(self, sizeinbits, offset, labels) # 16 bits long
        assert not isinstance(operands, str)
        AbstractInstr.__init__(self, inv_mnemonic, operands,
                               filepath, sourceline, 
                               sizeinbits, offset, labels)
                               
    @property
    def _instructionsize(self):
        return 16
    
    @property
    def isreadytoencode(self):
        return True
    
    def encodeToInt(self):
        word = 0
        for field in self.instrFields:
            if not isinstance(field, InstrField):
                import pprint
                print()
                print()
                print(type(self))
                pprint.pprint(vars(self))
                print("self = ", repr(self))
                print(type(field))
                print("field = ", repr(field))
                raise AssertionError("invalid InstrField instance")
            word <<= len(field)
            encoded = field.encodeToInt()
            if not 0 <= encoded < 2**len(field):
                raise RuntimeError("encoded instruction data "
                                   "out of acceptable range")
            word |= encoded
        if not word <= MAX_INSTR_VALUE:
            raise RuntimeError("encoded instruction data out of acceptable "
                               "range")
        return word

    def encodeToBinary(self):
        return super().encodeToBinary().zfill(16)


class RegInstr(HardwareInstr): # abstract class

    def __new__(cls, operands, *args, **kwargs):
        reg_src = operands[1].strip()
        if reg_src[0] == '$':
            raise SourceError("immediate found in register-type instruction")
        return super().__new__(cls) #, operands, filepath, sourceline)

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (reg_src, reg_dest) = operands
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_dest),
                       self.__class__.instrFields[2],
                       RegSpecifierField(reg_src))
        self.instrFields = instrFields
 
class NOP_Instr(HardwareInstr, metaclass=NOP_InstrType):

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        

class JmpBraLinkInstr(HardwareInstr): # abstract class
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (reg_link, reg_target_disp) = operands
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_link),
                       self.__class__.instrFields[2],
                       RegSpecifierField(reg_target_disp))
        self.instrFields = instrFields


class LSHI_Instr(HardwareInstr, metaclass=LSHI_InstrType):

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (immed, reg_dest) = operands
        immed_int = eval(immed.strip().strip('$'))
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_dest),
                       self.__class__.instrFields[2],
                       ShiftTypeField(0 if immed_int >= 0 else 1),
                       ShiftAmountField((immed_int)))
        self.instrFields = instrFields


class LabelOrImmedOperandAbstractInstr: # abstract class

    @property
    def hasRelocatableData(self):
        try:
            x = int(eval(self.operands[self.dataValueOperandNumber - 1]))
        except Exception as e:
            #print("exception from hasRelocatableData: " + repr(e))
            #print(repr(self.sourceline) + " DOES have relocatable data!")
            return True
        else:
            #print("no exception from hasRelocatableData")
            #print(repr(self.sourceline) + " DOES NOT have relocatable data!")
            #print("x is " + repr(x))
            return False

    @abstractproperty
    def datafieldisvalid(self):
        pass
    
    @abstractproperty
    def datafield(self): pass
    
    @datafield.setter
    def datafield(self, data): pass
    
    @property
    def hasunresolvedrefs(self):
        return not self.datafieldisvalid
    
    
    @property
    def isreadytoencode(self):
        return self.datafieldisvalid
        
    @abstractproperty
    def literalOperands(self):
        pass
    
    @abstractproperty
    def dataValueOperandNumber(self):
        """numbered from 1"""



# is sensitive to ordering of superclasses in class declaration
class LabelOrImmedOperandInstr(LabelOrImmedOperandAbstractInstr, 
                               HardwareInstr):
    pass


class Immed8bitInstr(LabelOrImmedOperandInstr): # abstract class

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (immed8bit, reg_dest) = operands
        instrFields = [self.__class__.instrFields[0],
                       RegSpecifierField(reg_dest),
                       None]
        self.instrFields = instrFields
        self.datafield = immed8bit # try to convert to immediate data field
    
    @property
    def datafieldisvalid(self):
        return isinstance(self.datafield, Immediate8bitDataField)
        
    @property
    def datafield(self):
        return self.instrFields[2]
    
    @datafield.setter
    def datafield(self, data):
        try:
            datafield = Immediate8bitDataField(data)
        except (SourceError, NameError, SyntaxError):
            self.instrFields[2] = data
        else:
            self.instrFields[2] = datafield
    
    @property
    def literalOperands(self):
        return (self.instrFields[2].formatAsOperand(), self.operands[1])
            
    @property
    def dataValueOperandNumber(self):
        """numbered from 1"""
        return 1

##class Immed12bitInstr(LabelOrImmedOperandInstr): # abstract class
##
##    def __init__(self, inv_mnemonic, operand,
##                 filepath=None, sourceline=None, labels=()):
##        super().__init__(inv_mnemonic, operand, filepath, sourceline, labels)
##        (mnemonics, immed12bit) = operand
##        instrFields = (self.__class__.instrFields[0],
##                       Immediate12bitDataField(immed12bit))
##        self.instrFields = instrFields














# r-type instructions

class ADD_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("add", "addw"),
                          mainOpcode=0b0000, extOpcode=0b0101):
    pass


class SUB_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("sub", "subw"),
                          mainOpcode=0b0000, extOpcode=0b1001):
    pass


class CMP_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("cmp", "cmpw"),
                          mainOpcode=0b0000, extOpcode=0b1011):
    pass


class AND_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("and", "andw"),
                          mainOpcode=0b0000, extOpcode=0b0001):
    pass


class OR_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("or", "orw"),
                         mainOpcode=0b0000, extOpcode=0b0010):
    pass


class XOR_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("xor", "xorw"),
                          mainOpcode=0b0000, extOpcode=0b0011):
    pass


class MOV_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("mov", "movw"),
                          mainOpcode=0b0000, extOpcode=0b1101):
    pass


class LSH_Instr(RegInstr, metaclass=RegInstrType, mnemonics=("lsh", "lshw"),
                          mainOpcode=0b1000, extOpcode=0b0100):
    pass





# i-type instructions

class ADDI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("addi",),
                                 mainOpcode=0b0101):
    pass


class SUBI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("subi",),
                                 mainOpcode=0b1001):
    pass


class CMPI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("cmpi",),
                                 mainOpcode=0b1011):
    pass


class ANDI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("andi",),
                                 mainOpcode=0b0001):
    pass


class ORI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                mnemonics=("ori",),
                                mainOpcode=0b0010):
    pass


class XORI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("xori",),
                                 mainOpcode=0b0011):
    pass


class MOVI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                 mnemonics=("movi",),
                                 mainOpcode=0b1101):
    pass


class LUI_Instr(Immed8bitInstr, metaclass=Immed8bitInstrType,
                                mnemonics=("lui",),
                                mainOpcode=0b1111):
    pass


##class MVIAT_Instr(Immed12bitInstr, metaclass=Immed12bitInstrType,
##                                   mnemonics=('mviat',),
##                                   mainOpcode=0b1110):
##    pass



# conditional setting/branching/jumping instructions

class Scond_Instr(HardwareInstr, metaclass=ScondInstrType):
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (reg_dest,) = operands
        cond = inv_mnemonic[1:].lower()
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_dest),
                       self.__class__.instrFields[2],
                       ConditionField(cond))
        self.instrFields = instrFields



class Jcond_Instr(HardwareInstr, metaclass=JcondInstrType):
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (reg_target,) = operands
        cond = inv_mnemonic[1:].lower()
        if cond == "mp": # as in jmp
            cond = "uc"
        instrFields = (self.__class__.instrFields[0],
                       ConditionField(cond),
                       self.__class__.instrFields[2],
                       RegSpecifierField(reg_target))
        self.instrFields = instrFields


class JAL_Instr(JmpBraLinkInstr, metaclass=RegInstrType, mnemonics=("jal",),
                                 mainOpcode=0b0100, extOpcode=0b1000):
    pass


class Bcond_Instr(LabelOrImmedOperandInstr, metaclass=BcondInstrType):

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        (displacement,) = operands
        cond = inv_mnemonic.strip()[1:].lower()
        if cond == "ra": # as in bra
            cond = "uc"
        self.instrFields = [self.__class__.instrFields[0],
                            ConditionField(cond),
                            None]
        self.datafield = displacement

    @property
    def datafieldisvalid(self):
        return isinstance(self.datafield, Immediate8bitDataField)
        
    @property
    def datafield(self):
        return self.instrFields[2]
    
    @datafield.setter
    def datafield(self, displacement):
        assert displacement is not None
        try:
            if isinstance(displacement, str):
                displacement_alt = eval(displacement.strip('$'))
            else:
                assert isinstance(displacement, int)
                displacement_alt = displacement
        except (ValueError, SyntaxError, NameError):
            self.instrFields[2] = displacement
        else:
            if displacement_alt % BYTESPERWORD != 0:
                raise SourceError("displacement given is not divisible by the "
                                 "word size")
            wordDisplacement = displacement_alt >> 1
            self.instrFields[2] = Immediate8bitDataField(wordDisplacement)
    
    @property
    def literalOperands(self):
        return (self.instrFields[2].formatAsOperand(),)
            
    @property
    def dataValueOperandNumber(self):
        """numbered from 1"""
        return 1
      


class BAL_Instr(LabelOrImmedOperandInstr, metaclass=BAL_InstrType):
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        if not (1 <= len(operands) <= 2):
            raise SourceError("wrong numbers of operands to BAL")
        if len(operands) == 1:
            (displacement,) = operands
        else:
            (linkreg, displacement) = operands
            if linkreg.strip().lower() != "ra":
                raise SourceError("incorrect link register format used")
        self.instrFields = [self.__class__.instrFields[0],
                            None]
        self.datafield = displacement

    @property
    def datafieldisvalid(self):
        return isinstance(self.datafield, Immediate8bitDataField)
        
    @property
    def datafield(self):
        return self.instrFields[1]
    
    @datafield.setter
    def datafield(self, displacement):
        assert displacement is not None
        try:
            if isinstance(displacement, str):
                displacement_alt = eval(displacement.strip('$'))
            else:
                assert isinstance(displacement, int)
                displacement_alt = displacement
        except (ValueError, SyntaxError, NameError):
            self.instrFields[1] = displacement
        else:
            if displacement_alt % BYTESPERWORD != 0:
                raise SourceError("displacement given is not divisible by the "
                                 "word size")
            wordDisplacement = displacement_alt >> 1
            self.instrFields[1] = Immediate8bitDataField(wordDisplacement)

    @property
    def literalOperands(self):
        return (self.instrFields[1].formatAsOperand(),)

    @property
    def dataValueOperandNumber(self):
        """numbered from 1"""
        return 1

                       
##class BcondL_Instr(HardwareInstr, metaclass=BcondLInstrType):
##
##    def __init__(self, operand, filepath=None, sourceline=None):
##        super().__init__(operand, filepath, sourceline)
##        (mnemonic, reg_disp) = operand
##        instrFields = (__class__.instrFields[0],
##                       ConditionField(mnemonic.strip()[1:-1].lower()),
##                       __class__.instrFields[2],
##                       RegSpecifierField(reg_disp))
##        self.instrFields = instrFields
##
##class BALL_Instr(JmpBraLinkInstr, metaclass=RegInstrType, mnemonics=("ball",),
##                                  mainOpcode=0b0100, extOpcode=0b1111):
##    pass



# load/store instructions

class LOAD_Instr(HardwareInstr, metaclass=LoadInstrType):

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        assert not isinstance(operands, str)
        (reg_addr, reg_dest) = operands
        operandsgood = (reg_addr[:1] == '(' or reg_addr[:2] == "0(") and \
                       reg_addr[-1] == ')'
        if not operandsgood:
            raise SourceError("indirection improperly specified for LOAD")
        reg_addr = reg_addr[1:-1] if (reg_addr[0] == '(') else reg_addr[2:-1]
        
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_dest),
                       self.__class__.instrFields[2],
                       RegSpecifierField(reg_addr))
        self.instrFields = instrFields


class STOR_Instr(HardwareInstr, metaclass=StorInstrType):

    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        super().__init__(inv_mnemonic, operands, filepath, sourceline,
                         offset, labels)
        assert not isinstance(operands, str)
        (reg_src, reg_addr) = operands
        operandsgood = (reg_addr[:1] == '(' or reg_addr[:2] == "0(") and \
                       reg_addr[-1] == ')'
        if not operandsgood:
            raise SourceError("indirection improperly specified for STOR")
        reg_addr = reg_addr[1:-1] if (reg_addr[0] == '(') else reg_addr[2:-1]
        
        instrFields = (self.__class__.instrFields[0],
                       RegSpecifierField(reg_src),
                       self.__class__.instrFields[2],
                       RegSpecifierField(reg_addr))
        self.instrFields = instrFields


##class LOADD_Instr(HardwareInstr, metaclass=LoaddInstrType):
##
##    def __init__(self, operands, filepath=None, sourceline=None):
##        super().__init__(operands, filepath, sourceline)
##        assert not isinstance(operands, str)
##        (mnemonic, reg_addrbase, reg_dest) = operands
##        assert reg_addrbase[:1] == '(' or reg_addrbase[:2] == "0("
##        assert reg_addrbase[-1] == ')'
##        reg_addrbase = reg_addrbase[1:-1] \
##                       if (reg_addrbase[0] == '(') else reg_addrbase[2:-1]
##        
##        instrFields = (self.__class__.instrFields[0],
##                       RegSpecifierField(reg_dest),
##                       self.__class__.instrFields[2],
##                       RegSpecifierField(reg_addrbase))
##        self.instrFields = instrFields
##
##
##class STORD_Instr(HardwareInstr, metaclass=StordInstrType):
##
##    def __init__(self, operands, filepath=None, sourceline=None):
##        super().__init__(operands, filepath, sourceline)
##        assert not isinstance(operands, str)
##        (mnemonic, reg_src, reg_addrbase) = operands
##        assert reg_addrbase[:1] == '(' or reg_addrbase[:2] == "0("
##        assert reg_addrbase[-1] == ')'
##        reg_addrbase = reg_addrbase[1:-1] \
##                       if (reg_addrbase[0] == '(') else reg_addrbase[2:-1]
##        
##        instrFields = (self.__class__.instrFields[0],
##                       RegSpecifierField(reg_src),
##                       self.__class__.instrFields[2],
##                       RegSpecifierField(reg_addrbase))
##        self.instrFields = instrFields











#### PSEUDO-INSTRUCTIONS ###

class MOVFI_Instr(LabelOrImmedOperandAbstractInstr, PseudoInstr,
                  metaclass=PseudoInstrType,
                  mnemonics=("movfi",), 
                  operandTypes=(IMMED_OPRND_T, REG_OPRND_T)):
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        PseudoInstr.__init__(self, inv_mnemonic, operands,
                             filepath, sourceline, 32, offset, labels)
        (immed16bit, self.reg_dest) = operands
        self.datafield = immed16bit # try to convert to immediate data field
    
    @property
    def datafieldisvalid(self):
        return isinstance(self.datafield16bit, Immediate16bitDataField)
        
    @property
    def datafield(self):
        return self.datafield16bit
    
    @datafield.setter
    def datafield(self, data):
        try:
            self.datafield16bit = Immediate16bitDataField(data)
        except SourceError:
            self.datafield16bit = data
            return
    
    
    def expandToHardwareInstrs(self):
        assert self.datafieldisvalid

        value = self.datafield.encodeToInt()
        #print("value for {} is {}".format(self.datafield, value))

#         if len(self.parts) == 3:
#             if self.parts[1] == "+":
#                 value = value + int(self.parts[2])
#             else:
#                 value = value - int(self.parts[2])
        dataupper = \
            Immediate8bitDataField( (value >> 8) & 0xFF )
        datalower = \
            Immediate8bitDataField( value & 0xFF )
        instr1 = LUI_Instr("lui", (hex(dataupper.encodeToInt()), 
                                   self.reg_dest))
        instr2 = ORI_Instr("ori", (hex(datalower.encodeToInt()), 
                                   self.reg_dest))
        
        return (instr1, instr2)
    
    @property
    def _instructionsize(self):
        return 32

    @property
    def literalOperands(self):
        raise NotImplementedError
    
    @property
    def dataValueOperandNumber(self):
        """numbered from 1"""
        return 1


class BSUB_Instr(PseudoInstr, LabelOrImmedOperandAbstractInstr,
                 metaclass=PseudoInstrType,
                 mnemonics=("bsub",), 
                 operandTypes=(REG_OPRND_T, IMMED_OPRND_T)):
    
    def __init__(self, inv_mnemonic, operands,
                 filepath=None, sourceline=None, offset=None, labels=()):
        PseudoInstr.__init__(self, inv_mnemonic, operands,
                             filepath, sourceline, self._instructionsize,
                             offset, labels)
        (self.reg_dest, immed16bit) = operands
        self.datafield = immed16bit # try to convert to immediate data field
    
    @property
    def datafieldisvalid(self):
        return isinstance(self.datafield16bit, Immediate16bitDataField)
        
    @property
    def datafield(self):
        return self.datafield16bit
    
    @datafield.setter
    def datafield(self, data):
        try:
            datafield = Immediate16bitDataField(data)
        except SourceError:
            self.datafield16bit = data
        else:
            self.datafield16bit = datafield
    
    def expandToHardwareInstrs(self):
        assert self.datafieldisvalid
        dataupper = \
            Immediate8bitDataField( (self.datafield.encodeToInt() >> 8) & 0xFF )
        datalower = \
            Immediate8bitDataField( self.datafield.encodeToInt() & 0xFF )
        instr1 = LUI_Instr("lui", (str(dataupper.encodeToInt()), 
                                   self.reg_dest))
        instr2 = ORI_Instr("ori", (str(datalower.encodeToInt()), 
                                   self.reg_dest))
        instr3 = JAL_Instr("jal", (self.reg_dest, self.reg_dest))
        return (instr1, instr2, instr3)
    
    @property
    def _instructionsize(self):
        return 48


# instruction aliases
addInstrToTable("bra", instrByMnemonic("buc"))
addInstrToTable("jmp", instrByMnemonic("juc"))






def unitTest_Instrs():
    def makeInstr(instr_cls, operands, *otherargs, mnemonic=None):
        if mnemonic == None:
            mnemonic = instr_cls.__name__[:instr_cls.__name__.index('_')]
            mnemonic = mnemonic.lower()
        return instr_cls(mnemonic, operands, *otherargs)

    i = makeInstr

    x = (
        i(ADD_Instr, ("r3", "r5")),
        i(ADDI_Instr, ("$0x20", "r0")),
        i(SUB_Instr, ("r8", "r1")),
        i(SUBI_Instr, ("$-128", "r12")),
        i(CMP_Instr, ("r3", "r10")),
        i(CMPI_Instr, ("$0xFF", "r15")),
        i(AND_Instr, ("r0", "r1")),
        i(ANDI_Instr, ("12", "r9")),
        i(OR_Instr, ("r0", "r0")), # NOP
        i(ORI_Instr, ("$0", "r13"), None, 20, 25, "gulp"),
        i(XOR_Instr, ("r12", "r7")),
        i(XORI_Instr, ("$0x18", "r2")),
        i(MOV_Instr, ("r10", "r12")),
        i(MOVI_Instr, ("10", "r0")),
        i(LSH_Instr, ("r3", "r9")),
        i(LSHI_Instr, ("8", "r14")), 
        i(LUI_Instr, ("$200", "r12")),
        i(LOAD_Instr, ("(r8)", "r13")),
        #i(LOADD_Instr, ("(r6)", "r2")),
        i(STOR_Instr, ("r2", "(r6)")),
        #i(STORD_Instr, ("r6", "(r6)")),
        i(Bcond_Instr, ("$0x128",), mnemonic="beq"),
        i(BAL_Instr, ("ra", "$0xC2")),
        i(Jcond_Instr, ("r2",), mnemonic="jhi"),
        i(JAL_Instr, ("ra", "r6")),
        i(BAL_Instr, ("ra", "$0x2")),
        i(Scond_Instr, ("r0",), mnemonic="seq"),
        i(MOVFI_Instr, ("$0x0400", "r9")),
        i(BSUB_Instr, ("r12", "$0x40"))
        #i(MVIAT_Instr, ("256",)),
        #i(BALL_Instr, ("r7", "r0")),
        #i(BcondL_Instr, ("r10",), mnemonic="bccl")
    )
    y = []
    for a in x:
        if a.sizeinwords > 1:
            for b in a.expandToHardwareInstrs():
                y.append(b.encodeToBinary())
        else:
            y.append(a.encodeToBinary())
    pprint(x)
    print('\n' + str(len(x)) + '\n')
    pprint(y)
    print('\n' + str(len(y)) + '\n')
    

    return x, y

##unitTest_Instrs()

##verbatimInstrTable = dict( (i.mnemonics, i) for i in _verbatimInstrs )
##regexInstrTable = dict( (i.mnemonics, i) for i in _regexInstrs )
##fullInstrTable = dict( (i.mnemonics, i) for i in allInstrs )

# tells what should be imported by "from instructiondata import *"
##__all__ = "verbatimInstrTable, regexInstrTable, allInstrs".split(", ")
##__all__ += [ cls.__name__ for cls in allInstrs ]

if __name__ == "__main__":
#    pprint(verbatimInstrTable)
#    pprint(regexInstrTable)
#    pprint(__all__)
    pprint(getInstrTable())
    unitTest_Instrs()

