from myhdl import Signal
from enum import Enum, EnumItem

from .util import CachingType, IndexBy1List, IRange, StateObject

from .pitch import Pitch, OctaveName


__all__ = ["RanknoteInfo",
           "BlankRanknoteInfo",
           "SoundingRanknoteInfo",
           "ToneProducer",
           "PipeToneProducer",
           "PercussionToneProducer",
           "NonTunedPercussionRanknoteInfo",
           "TunedSoundingRanknoteInfo",
           "PipeRanknoteInfo",
           "TunedPercussionRanknoteInfo",
           "RankInfo",
           "maketunedranks", 
           "VoiceInfo", 
           "VoicenoteState", 
           "VoiceState",
           "TotalVoicesState",
           ]


class RanknoteInfo:
    """
    an abstract class, concrete instances of which contain basic information 
    on a ranknote
    
    A ranknote is at the intersection of a voicenote from a voice and a rank 
    from the same voice, like a traffic intersection of an east-west street and
    a north-south street in a city with a grid system.
    
    See document on __init__ for information on how to initialize.
    
    Instance attributes:
    
    notenum: (int) which note number in its containing voice this ranknote is 
                   in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote
    """
    
    __slots__ = ["voicenum",
                 "voicename", 
                 "notenum",
                 "ranknum"]
    
    def __new__(cls, *args, **kwargs):
        if cls is RanknoteInfo:
            raise TypeError("class RanknoteInfo is abstract and "
                            "should not be directly instantiated")
        return super().__new__(cls)
        
    def __init__(self, voicenum, voicename, notenum, ranknum=1):
        """
        RanknoteInfo.__init__(self, voicenum, voicename, notenum [, ranknum])

        voicenum (int): the organ-wide unique number assigned to the voice 
                        containing this voice component
        voicename (str): the name of the voice to which this voice component 
                         belongs
        notenum (int): the note number of this voice component in its voice
        ranknum (int): the rank of the voice to which this voice component 
                       belongs (optional; defaults to 1) 
        """
        self.voicenum = voicenum
        self.voicename = voicename
        self.notenum = notenum
        self.ranknum = ranknum


class BlankRanknoteInfo(RanknoteInfo):
    """
    BlankRanknoteInfo(voicenum, voicename, notenum [, ranknum]) -> instance
    
    a ranknote characterized by the absence of any noise-producing apparatus  
    (neither pipe nor percussion, electronic or acoustic, sounding or muted)
    
    voicenum (int): the organ-wide unique number assigned to the voice 
                    containing this voice component
    voicename (str): the name of the voice to which this voice component 
                     belongs
    notenum (int): the note number of this voice component in its voice
    ranknum (int): the rank of the voice to which this voice component belongs
                   (optional; defaults to 1) 
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote
    """
    
    __slots__ = []
    
    def __init__(self, voicenum, voicename, notenum, ranknum=1):
        super().__init__(voicenum, voicename, notenum, ranknum)


class SoundingRanknoteInfo(RanknoteInfo):
    """
    an abstract class, concrete instances of which contain info on a ranknote
    with a noise-making apparatus, capable of producing sound (this may even be
    a muted apparatus whose noise-making ability has been disabled such as a
    pipe that has been muted in place)
    
    A sounding ranknote is basically just a ranknote that can produce sound 
    (blank ranknotes have no sound producing component).  All sounding 
    ranknotes can produce sound, either through a pipe of some kind, or 
    percussion (which may be tuned or untuned; tuned percussion have a 
    discernable pitch while untuned percussion do not).
    
    See documentation on __init__ for initialization information.
    
    Instance attributes:
    
    iselectronic: (bool) true if this ranknote is electronically synthesized
    ismuted: (bool) whether this sounding ranknote has been muted; this is 
                    useful for keeping track of pipework that has been muted in
                    place or is non-functional.
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote
    """
    
    __slots__ = ["iselectronic", "ismuted"]
        
    def __new__(cls, *args, **kwargs):
        if cls is SoundingRanknoteInfo:
            raise TypeError("SoundingRanknoteInfo is abstract and should not "
                            "be directly instantiated")
        return super().__new__(cls, *args, **kwargs)
    
    def __init__(self, voicenum, voicename, notenum, *, 
                 ranknum=1, iselectronic=False, ismuted=False):
        """
        SoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum
                                      [, ranknum=<obj1>] 
                                      [, iselectronic=<obj2>]
                                      [, ismuted=<obj3>])
        
        voicenum (int): the organ-wide unique number assigned to the voice 
                        containing this voice component
        voicename (str): the name of the voice to which this voice component 
                         belongs
        notenum (int): the note number of this voice component in its voice
        ranknum (int): the rank of the voice to which this voice component 
                       belongs (optional; defaults to 1) 
        iselectronic (bool): whether this voice component speaks electronically 
                             through a loudspeaker (optional; defaults to 
                             False)
        ismuted (bool): True if this "sounding" ranknote does not play; note,
                        a blank is a void where no sound component exists to 
                        play it; a muted sounding ranknote has a means of 
                        sound production but it is disabled  (optional; 
                        defaults to False)
        """
        assert isinstance(voicenum, int)
        assert isinstance(voicename, str)
        assert isinstance(notenum, int)
        assert isinstance(ranknum, int)
        assert isinstance(iselectronic, bool)
        assert isinstance(ismuted, bool)
        
        super().__init__(voicenum, voicename, notenum, ranknum)
        
        self.iselectronic = iselectronic
        self.ismuted = ismuted
    
    def __repr__(self):
        pattern = "{clsname}({self.voicenum}, {self.voicename!r}, " \
                  "{self.notenum}, ranknum={self.ranknum}, " \
                  "iselectronic={self.iselectronic}, ismuted={self.ismuted})"
        return pattern.format(clsname=type(self).__name__, self=self)


class ToneProducer:
    """
    ToneProducer is an abstract class.
    
    Members of subclasses of this class represent apparata which can produce 
    sound by one means or another.
    
    Concrete subclasses of SoundingRanknoteInfo mark that their instances all
    produce tone in a certain way by multiply inheriting from a subclass of
    ToneProducer.  Using marker classes in this way relieves the instances of
    these classes from having to carry extra attributes as baggage, because the
    information is implicitly stored in the object's __class__ field.  
    """
    
    __slots__ = []
    
    def __init__(self):
        """ToneProducer.__init__(self)"""
        if type(self) is ToneProducer:
            raise TypeError("class ToneProducer is abstract and should not be "
                            "directly instantiated")


class PipeToneProducer(ToneProducer):
    """
    abstract marker class for apparata for producing sound using pipes (either 
    flues or reeds)
    """
    
    __slots__ = []
    
    def __init__(self):
        """PipeToneProducer.__init__(self)"""
        if type(self) is PipeToneProducer:
            raise TypeError("class PipeToneProucer is abstract and should not "
                            "be directly instantiated")


class PercussionToneProducer(ToneProducer):
    """
    abstract marker class for apparata for producing sound using percussion 
    (two things striking one another)
    """
    
    __slots__ = []
    
    def __init__(self):
        """PercussionToneProducer.__init__(self)"""
        if type(self) is PercussionToneProducer:
            raise TypeError("class PercussionToneProducer is abstract and "
                            "should not be directly instantiated")


class NonTunedPercussionRanknoteInfo(SoundingRanknoteInfo, 
                                     PercussionToneProducer):
    """
    NonTunedPercussionRanknoteInfo(voicenum, voicename, notenum,
                                   [, ranknum=ranknum] 
                                   [, iselectronic=iselectronic]
                                   [, ismuted=ismuted])
        -> instance
    
    info on a single non-tuned percussion apparatus
    
    voicenum (int): the organ-wide unique number assigned to the voice 
                    containing this voice component
    voicename (str): the name of the voice to which this voice component 
                     belongs
    notenum (int): the note number of this voice component in its voice
    soundingpitch (Pitch): the closest actual fundamental pitch to that 
                           sounded by this tuned sounding ranknote
    ranknum (int): the rank of the voice to which this voice component 
                   belongs (optional; defaults to 1) 
    iselectronic (bool): whether this voice component speaks electronically 
                         through a loudspeaker (optional; defaults to 
                         False)
    ismuted (bool): True if this "sounding" ranknote does not play; note, a 
                    blank is a void where no sound component exists to play 
                    it; a muted sounding ranknote has a means of sound 
                    production but it is disabled  (optional; defaults to 
                    False)
    
    Instance attributes inherited from SoundingRanknoteInfo:
    
    iselectronic: (bool) true if this ranknote is electronically synthesized
    ismuted: (bool) whether this sounding ranknote has been muted; this is 
                    useful for keeping track of pipework that has been muted in
                    place or is non-functional.
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote

    """
    
    __slots__ = []
    
    def __init__(self, voicenum, voicename, notenum, *, 
                 ranknum=1, iselectronic=False, ismuted=False):
        SoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum,
                                      ranknum=ranknum, 
                                      iselectronic=iselectronic,
                                      ismuted=ismuted)
        PercussionToneProducer.__init__(self)


class TunedSoundingRanknoteInfo(SoundingRanknoteInfo):
    """
    abstract class representing sounding ranknotes with a tuned, discernable
    pitch
    
    See documentation on __init__ for initialization information.
    
    Instance attributes:

    soundingpitch (Pitch): the closest actual fundamental pitch to that sounded
                           by this tuned sounding ranknote
    
    Instance attributes inherited from SoundingRanknoteInfo:
    
    iselectronic: (bool) true if this ranknote is electronically synthesized
    ismuted: (bool) whether this sounding ranknote has been muted; this is 
                    useful for keeping track of pipework that has been muted in
                    place or is non-functional.
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote
    """
    
    __slots__ = ["soundingpitch"]
    
    def __new__(cls, voicenum, voicename, notenum, soundingpitch, **kwargs):
        if cls is SoundingRanknoteInfo:
            raise TypeError("TunedSoundingRanknoteInfo is abstract and should "
                            "not be directly instantiated")
        return super().__new__(cls, voicenum, voicename, notenum, **kwargs)
    
    def __init__(self, voicenum, voicename, notenum, soundingpitch, *, 
                 ranknum=1, iselectronic=False, ismuted=False):
        """
        TunedSoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum, 
                                           soundingpitch
                                           [, ranknum=ranknum] 
                                           [, iselectronic=iselectronic]
                                           [, ismuted=ismuted])
    
        a sounding ranknote with a perceivable fundamental pitch
        
        voicenum (int): the organ-wide unique number assigned to the voice 
                        containing this voice component
        voicename (str): the name of the voice to which this voice component 
                         belongs
        notenum (int): the note number of this voice component in its voice
        soundingpitch (Pitch): the closest actual fundamental pitch to that 
                               sounded by this tuned sounding ranknote
        ranknum (int): the rank of the voice to which this voice component 
                       belongs (optional; defaults to 1) 
        iselectronic (bool): whether this voice component speaks electronically 
                             through a loudspeaker (optional; defaults to 
                             False)
        ismuted (bool): True if this "sounding" ranknote does not play; note, a 
                        blank is a void where no sound component exists to play 
                        it; a muted sounding ranknote has a means of sound 
                        production but it is disabled  (optional; defaults to 
                        False)
        """
        SoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum, 
                                      ranknum=ranknum, 
                                      iselectronic=iselectronic,
                                      ismuted=ismuted)
        assert isinstance(soundingpitch, Pitch)
        self.soundingpitch = soundingpitch
    

class PipeRanknoteInfo(TunedSoundingRanknoteInfo, PipeToneProducer):
    """
    PipeRanknoteInfo(voicenum, voicename, notenum, soundingpitch
                     [, ranknum=ranknum] [, iselectronic=iselectronic]
                     [, ismuted=ismuted])
        -> ranknote
    
    info on a single pipe
    
    voicenum (int): the organ-wide unique number assigned to the voice 
                    containing this voice component
    voicename (str): the name of the voice to which this voice component 
                     belongs
    notenum (int): the note number of this voice component in its voice
    soundingpitch (Pitch): the closest actual fundamental pitch to that 
                           sounded by this tuned sounding ranknote
    ranknum (int): the rank of the voice to which this voice component 
                   belongs (optional; defaults to 1) 
    iselectronic (bool): whether this voice component speaks electronically 
                         through a loudspeaker (optional; defaults to 
                         False)
    ismuted (bool): True if this "sounding" ranknote does not play; note, a 
                    blank is a void where no sound component exists to play 
                    it; a muted sounding ranknote has a means of sound 
                    production but it is disabled  (optional; defaults to 
                    False)

    Instance attributes from TunedSoundingRanknoteInfo:

    soundingpitch (Pitch): the closest actual fundamental pitch to that sounded
                           by this tuned sounding ranknote
    
    Instance attributes inherited from SoundingRanknoteInfo:
    
    iselectronic: (bool) true if this ranknote is electronically synthesized
    ismuted: (bool) whether this sounding ranknote has been muted; this is 
                    useful for keeping track of pipework that has been muted in
                    place or is non-functional.
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote
    """
    
    __slots__ = []
    
    def __init__(self, voicenum, voicename, notenum, soundingpitch, *, 
                 ranknum=1, iselectronic=False, ismuted=False):
        TunedSoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum,
                                           soundingpitch,
                                           ranknum=ranknum,
                                           iselectronic=iselectronic,
                                           ismuted=ismuted)
        PipeToneProducer.__init__(self)


class TunedPercussionRanknoteInfo(TunedSoundingRanknoteInfo,
                                  PercussionToneProducer):
    """
    TunedPercussionRanknoteInfo(voicenum, voicename, notenum, soundingpitch
             [, ranknum=ranknum] [, iselectronic=iselectronic]
             [, ismuted=ismuted])
        -> ranknote
    
    info on a single pipe
    
    voicenum (int): the organ-wide unique number assigned to the voice 
                    containing this voice component
    voicename (str): the name of the voice to which this voice component 
                     belongs
    notenum (int): the note number of this voice component in its voice
    soundingpitch (Pitch): the closest actual fundamental pitch to that 
                           sounded by this tuned sounding ranknote
    ranknum (int): the rank of the voice to which this voice component 
                   belongs (optional; defaults to 1) 
    iselectronic (bool): whether this voice component speaks electronically 
                         through a loudspeaker (optional; defaults to 
                         False)
    ismuted (bool): True if this "sounding" ranknote does not play; note, a 
                    blank is a void where no sound component exists to play 
                    it; a muted sounding ranknote has a means of sound 
                    production but it is disabled  (optional; defaults to 
                    False)
    
    Instance attributes from TunedSoundingRanknoteInfo:

    soundingpitch (Pitch): the closest actual fundamental pitch to that sounded
                           by this tuned sounding ranknote
    
    Instance attributes inherited from SoundingRanknoteInfo:
    
    iselectronic: (bool) true if this ranknote is electronically synthesized
    ismuted: (bool) whether this sounding ranknote has been muted; this is 
                    useful for keeping track of pipework that has been muted in
                    place or is non-functional.
    
    Instance attributes inherited from RanknoteInfo:
    
    notenum: (int) which note number its containing voice this ranknote is in
    ranknum: (int) the number of the rank in its ranknote that this voice
                   component is in
    voicenum: (int) the number of the voice containing this ranknote
    voicename: (str) the name of the voice containing this ranknote

    """
    
    __slots__ = []
    
    def __init__(self, voicenum, voicename, notenum, soundingpitch, *, 
                 ranknum=1, iselectronic=False, ismuted=False):
        TunedSoundingRanknoteInfo.__init__(self, voicenum, voicename, notenum,
                                           soundingpitch,
                                           ranknum=ranknum,
                                           iselectronic=iselectronic,
                                           ismuted=ismuted)
        PercussionToneProducer.__init__(self)


class RanknoteCount:
    """
    RanknoteCount(unmutedpipes [, mutedpipes 
                                [, unmutedpercussion]
                                 [, mutedpercussion]
                                  [, blanks])
        -> instance
    
    unmutedpipes: (int) pipes that aren't muted
    mutedpipes: (int) pipes that are muted
    unmutedpercussion: (int) percussion that aren't muted
    mutedpercussion: (int) percussion that are muted
    blanks: (int) blanks
    
    class to hold the result of counting up ranknotes
    
    Instance attributes:
    
    unmutedpipes: (int) described above
    mutedpipes: (int) described above
    unmutedpercussion: (int) described above
    mutedpercussion: (int) described above
    blanks: (int) described above
    """
    
    __slots__ = ["unmutedpipes", 
                 "mutedpipes", 
                 "unmutedpercussion", 
                 "mutedpercussion", 
                 "blanks"]
    
    def __init__(self, unmutedpipes, mutedpipes=0, 
                       unmutedpercussion=0, mutedpercussion=0,
                       blanks=0):
        self.unmutedpipes = unmutedpipes
        self.mutedpipes = mutedpipes
        self.unmutedpercussion = unmutedpercussion
        self.mutedpercussion = mutedpercussion
        self.blanks = blanks
    
    def __add__(self, other):
        return RanknoteCount(self.unmutedpipe + other.unmutedpipes,
                             self.mutedpipes + other.mutedpipes,
                             self.unmutedpercussion + other.unmutedpercussion,
                             self.mutedpercussion + other.mutedpercussion,
                             self.blanks + other.blanks)

    @property
    def allpipes(self):
        """(int) total muted and unmuted pipes"""
        return self.unmutedpipes + self.mutedpipes
    
    @property
    def allpercussion(self):
        """(int) total muted and unmuted percussion"""
        return self.unmutedpercussion + self.mutedpercussion
    
    @property
    def allunmuted(self):
        """(int) all unmuted pipes and percussion"""
        return self.unmutedpipes + self.unmutedpercussion
    
    @property
    def allmuted(self):
        """(int) all muted pipes and percussion"""
        return self.mutedpipes + self.mutedpercussion
    
    @property
    def soundcapable(self):
        """
        (int) all muted or unmuted piper or percussion (all non-blank 
              ranknotes)
        """
        return (self.unmutedpipes + self.mutedpipes + 
                self.unmutedpercussion + self.mutedpercussion)
    
    @property
    def ranknotes(self):
        """
        (int) total ranknotes of every kind, including un/muted pipe/percussion
              and blank
        """
        return (self.unmutedpipes + self.mutedpipes + 
                self.unmutedpercussion + self.mutedpercussion +
                self.blanks)
    

class RankInfo:
    """
    RankInfo(ranknum, voicenum, voicename, ranknoteinfos) -> rankinfo
    
    a class to represent each rank in a voice
    
    ranknum (int): the number of the rank within its voice
    voicenum (int): the application-wide unique voice id of the voice 
                    containing this rank
    voicename (str): the name of the voice containing this rank
    ranknoteinfos (iterable of RanknoteInfo): the ranknotes that will form this
                                              rank
    
    The concept of a rank is explained in the module documentation for the 
    package <organsim>.
    
    A RankInfo MUST have the same number of RanknoteInfo instances in 
    ranknoteinfos as the numnotes property of the VoiceInfo to which it 
    belongs.
    
    Instance attributes:
    ranknum: (int) the rank number of this rank within its containing voice, 
                   usually between 1 and 6
    voicenum: (int) the organ-wide unique number of the voice that owns this 
                    rank
    voicename: (str) the name of the voice that owns this rank
    ranknoteinfos: (IndexBy1List) a container for the ranknotes comprising this
                                  rank
    """
    
    __slots__ = ["ranknum",
                 "voicenum",
                 "voicename",
                 "ranknoteinfos"]
    
    def __init__(self, ranknum, voicenum, voicename, ranknoteinfos):
        self.ranknum = ranknum
        self.voicenum = voicenum
        self.voicename = voicename
        self.ranknoteinfos = IndexBy1List(ranknoteinfos)
        
    def __repr__(self):
        pattern = "<rank {} of voice {} named {!r} with {} notes from {}"
        return pattern.format(self.ranknum, 
                              self.voicenum,
                              self.voicename,
                              len(self.ranknoteinfos),
                              self.lowestpitch())

    def lowestpitch(self):
        """
        return the sounding pitch of the voice component with the lowest 
        note number or None if all the ranknotes are blanks
        """
        for ranknoteinfo in self.ranknoteinfos:
            if not type(ranknoteinfo).allinstancesareblanks:
                return ranknoteinfo.soundingpitch
        return None
    
    def countranknotes(self):
        """
        rankinfo.countranknotes([excludemuted]) -> (tuple)
        
        counts the sounding ranknotes in this rank
        
        excludemuted (bool): whether to not count ranknotes that are muted
        
        returns tuple of (countsoundingranknotes, countpipes, countpercussion)
        """
        unmutedpipes = mutedpipes = 0
        unmutedpercussion = mutedpercussion = 0
        blanks = 0
        for ranknote in self.ranknoteinfos:
            if isinstance(ranknote, BlankRanknoteInfo):
                blanks += 1
            assert isinstance(ranknote, SoundingRanknoteInfo)
            if isinstance(ranknote, PipeToneProducer):
                if ranknote.ismuted:
                    mutedpipes += 1
                else:
                    unmutedpipes += 1
            else:
                assert isinstance(ranknote, PercussionToneProducer)
                if ranknote.ismuted:
                    mutedpercussion += 1
                else:
                    unmutedpercussion += 1
        return RanknoteCount(unmutedpipes, mutedpipes,
                             unmutedpercussion, mutedpercussion,
                             blanks)


def maketunedranks(voicenum, voicename, refoctave, intervalsbyrank, *, 
                   iselectronic, fromclass=PipeRanknoteInfo):
    """
    maketunedranks(voicenum, voicename, refoctave, intervalsbyrank,
                   iselectronic=iselectronic [, fromclass=fromclass]) 
        -> list of RankInfo
    
    make ranks of ranknotes
    
    voicenum: (int) the number of the voice to which these ranks of voice 
                    components will belong
    voicename: (str) the name of the voice
    refoctave: (OctaveName) must be either OctaveName._8_FOOT or
                            OctaveName._16_FOOT
    intervalsbyrank: (iterable of list of int) each interior list is a sequence 
                                               of numbers represents the 
                                               diatonic intervals corresponding 
                                               to each note; if the note is 
                                               negative then a muted ranknote
                                               is generated
    iselectronic: (bool) True if all voice componets are electronic, False if
                         all voice components are non-electronic, or a list of
                         note numbers of which notes are electronically sounded
    fromclass: (subclass of VoiceComponentInfo) to be used to instantiate the 
                                                voice components (optional;
                                                defaults to PipeRanknoteInfo)
    
    example:
    A mixture with voicenum 14, voicename "2' Fourniture—IV ranks", 
    refoctave OctaveName._8_FOOT, and the following composition:
    
    15-19-22-26:  18 notes, starting at Low C
    12-15-19-22:  24 notes
     8-12-15-19:  12 notes
     1- 8-12-15:   7 notes
     
    In the above composition, each column represents a rank.  Because there are
    4 columns, there are four ranks.
    
    Each row represents a set of intervals (one interval for each rank) which 
    set is repeated over the indicated number of voicenotes.
    
    The above chart is really a condensed form of this:
    
        NOTE#            RANK1    RANK2    RANK3    RANK4    [REPEAT#]
        1 (Low C)        15th     19th     22nd     26th     1
        2 (Low C#)       15th     19th     22nd     26th     2
        .                .        .        .        .        .
        .                .        .        .        .        .
        .                .        .        .        .        .
        18 (Tenor F)     15th     19th     22nd     26th     18
        
        19 (Tenor F#)    12th     15th     19th     22nd     1
        .                .        .        .        .        .
        .                .        .        .        .        .
        .                .        .        .        .        .
        42 (Treble F)    12th     15th     19th     22nd     24

        43 (Treble F#)   octave   12th     15th     19th     1
        .                .        .        .        .        .
        .                .        .        .        .        .
        .                .        .        .        .        .
        54 (Soprano F)   octave   12th     15       19th     12
        
        55 (Soprano F#)  unison   octave   12th     15th     1
        .                .        .        .        .        .
        .                .        .        .        .        .
        .                .        .        .        .        .
        61 (Top C)       unison   octave   12th     15th     7
    
    Now you have to turn it on its side so that each row is a rank and the
    voicenotes go across.  This is how you create that same mixture with the
    makevoicecomps function:
    
        maketunedranks(14, "2' Fourniture—IV ranks", OctaveName._8_FOOT
                       [[15]*18 + [12]*24 + [8]*12 + [1]*7,
                        [19]*18 + [15]*24 + [12]*12 + [8]*7,
                        [22]*18 + [19]*24 + [15]*12 + [12]*7,
                        [26]*18 + [22]*24 + [19]*12 + [15]*7],
                       iselectronic=True)
    """
    # maps diatonic intervals to chromatic displacements
    intervaldict = {1: 0,
                    5: 7,
                    8: 12,
                    12: 19,
                    15: 24,
                    17: 28,
                    19: 31,
                    22: 36,
                    26: 43,
                    29: 48,
                    33: 55,
                    36: 60}
    
    octavetobasepitch = {OctaveName._8_FOOT: Pitch("CC"),
                         OctaveName._16_FOOT: Pitch("CCC")}
    
    try:
        basepitch = octavetobasepitch[refoctave]
    except KeyError:
        raise ValueError("refoctave must be an OctaveName of either 16' "
                         "or 8' pitch")
    
    ranks = []
    for ranknumminus1, rankintervals in enumerate(intervalsbyrank):
        ranknotes = []
        for notenum in range(1, len(rankintervals) + 1):
            noteindex = notenum - 1
            
            curinterval = rankintervals[noteindex]
            if curinterval < 0:
                ismuted = True
                curinterval = -curinterval
            else:
                ismuted = False
            
            pitchdisplacement = intervaldict[curinterval]
            
            if isinstance(iselectronic, bool):
                pipeiselec = iselectronic
            elif isinstance(iselectronic, list):
                pipeiselec = notenum in iselectronic
            else:
                raise TypeError("iselectronic must be bool or list of note "
                                "numbers")
            assert isinstance(pipeiselec, bool)
            
            ranknotes.append(fromclass(voicenum, voicename, notenum, 
                                       basepitch + pitchdisplacement + 
                                            noteindex,
                                       ranknum=ranknumminus1 + 1, 
                                       iselectronic=pipeiselec,
                                       ismuted=ismuted))
                                 
        assert len(ranknotes) == len(rankintervals)
        ranks.append(RankInfo(ranknumminus1 + 1,
                              voicenum,
                              voicename,
                              ranknotes))
    
    return ranks
    

class VoiceInfo(metaclass=CachingType):
    """
    VoiceInfo(voicenum, name, divisionname, numnotes, rankinfos
              [, primaryregoffset]
              *,
              [, iselectronic] [, isvirtual] [, istunedpercussion])
    
    Arguments after the "*," are keyword-only parameters each of whose whose 
    argument values must be prefixed by PARAMETER= (replace PARAMETER with the 
    actual name of each parameter).  The "*," is not actually typed when 
    calling the constructor; it only serves to separate non-keyword parameters
    from keyword parameters.  Parameters listed between square brackets, both
    the positional and keyword varieties, are optional and may be omitted.
    
    
    voicenum (int): the voice number
    name (str): the name of the voice
    divisionname (str): the name of the VOICE/PHYSICAL division to which the 
                        voice belongs, for documentation purposes; this may be 
                        None, if absolutely expedient
    numnotes (int): the number of notes in the voice
    rankinfos (<special>): iterable of RankInfo objects OR a Pitch object 
                           giving the lowest starting pitch a single pipe rank
                           which RankInfo will be automatically created--this
                           should be None if <isvirtual> is True
    primaryregoffset (int): the offset in notes to the note where begins the
                            voice's primary register, which is loosely defined
                            as either the lowest register where the lowest keys
                            on the keyboard are mapped to sounding voicenotes
                            OR the register that will receive the most use;
                            see "HOW TO CALCUTAE primaryregoffset" below.
                            primaryregoffset is slightly subjective, but helps
                            to identify the voice.  (optional; default is 0)
    iselectronic (bool or iterable): False if all ranknotes in the voice are 
                                     non-electronic, True if all ranknotes 
                                     completely electronic, or an iterable 
                                     (such as a range object) yielding the note 
                                     numbers that contain electronic ranknotes 
                                     (optional; default False)
    isvirtual (bool): if the voice is virtual, then it is a voice has no 
                      ranknotes of its own, but rather maps its notes to and is
                      implemented by other voices which are non-virtual 
                      (optional; default False)
    istunedpercussion (bool): when True and when <rankinfos> is a Pitch object,
                              a voice with a single rank of tuned percussion 
                              ranknotes will be created instead of one with 
                              pipe ranknotes. (optional; default is False) 

    HOW TO CALCULATE primaryregoffset:
    
    If the voice has no more or fewer voicenotes than the number of keys on the 
        keyboard the voice is intended to be played on, primaryregoffset is 0, 
        and you're done;
    else if the voice has MORE voicenotes than the keyboard it is to be played
        on, but the most important or principal register derived from this 
        voice is that which maps the lowest note of the voice to the lowest key
        of the keyboard, then primaryregoffset is still 0, and you'r done;
    else if the voice has MORE voicenotes than the keyboard it is to be played
        on but the most important or principal register derived from this
        voice is that which, in mapping notes from the keyboard to the voice, 
        skips the lowest <X> notes of the voice before mapping note <X> + 1
        of the voice to the lowest note of the keyboard, then primaryregoffset
        is <X> and you're done.
    else if the voice has <X> fewer voicenotes than the keyboard the voice is 
        intended to be played on and the first <X> keys at the bottom of the 
        keyboard are to be silent (with the next key up sounding note 1 of the
        voice) then primaryregoffset is negative <X> and you're done;
    else you must be doing something very unusual; hopefully you know what 
        you're doing, and if so, you can figure out the right number yourself.
        
    EXAMPLES OF primaryregoffset
        
    For instance, a value of zero means the lowest note maps to the lowest
        key in the primary register of the voice.  This is the most common.
    A value of 12 means that the voice has a suboctave extension below
        the primary register, and one must skip 12 notes from note 1,
        arriving at note 13 as the note that maps to the lowest key C in 
        the primary register of the voice.
    Finally, a value of -12 means that the voice is missing a bottom
        octave and that the note 1 of the voice maps to the second lowest 
        key C in the primary register of the voice.
    
    Instance attributes:
    
    voicenum: (int) the number of the voice
    name: (str) the name of the voice
    divisionname: (str) the name of the division in which this voice is located
    numnotes: (int) the number of notes implemented by this voice
    rankinfos: (IndexBy1List of RankInfo) the rankinfos
    primaryregoffset: (int) the primary register offset
        See the class documentation for a discussion what the primary register
        offset is and how to determine what it should be.
    iselectronic: (bool or iterable or range object) 
        to what degree the voice is electronic
        
        False if the voice is completely non-electronic
        True if the voice is completely electronic
        a range object or iterable yielding the note numbers that 
            are electronic means the voice is a mixture of electronic and 
            non-electronic componentns  (default False)
    isvirtual: (bool) whether the voice is virtual
        If the voice is virtual then it is a "soft" voice (soft like in 
        software) that maps to and is implemented by other non-virtual or 
        "hard" voices
    """
    
    __slots__ = ["voicenum", 
                 "name", 
                 "divisionname",
                 "numnotes",
                 "rankinfos",
                 "primaryregoffset",
                 "iselectronic",
                 "isvirtual"]
        
    def __init__(self, voicenum, name, divisionname, numnotes, rankinfos, 
                 primaryregoffset=0, *,
                 iselectronic=None, isvirtual=False, istunedpercussion=False):
        self.voicenum = voicenum
        self.name = name
        self.divisionname = divisionname
        self.numnotes = numnotes
        
        self.primaryregoffset = primaryregoffset
        
        iselectronic = iselectronic or False
        self.iselectronic = iselectronic
        
        self.isvirtual = isvirtual
        
        if isinstance(rankinfos, Pitch):
            startpitch = rankinfos
            classtomake = PipeRanknoteInfo if not istunedpercussion else \
                          TunedPercussionRanknoteInfo
            try:
                rank1voicecomps = [classtomake(voicenum=voicenum, 
                                               voicename=name, 
                                               notenum=notenum, 
                                               soundingpitch=startpitch + 
                                                             notenum - 1,
                                               ranknum=1,
                                               iselectronic=iselectronic 
                                                    if isinstance(iselectronic, 
                                                                  bool) 
                                                    else notenum in 
                                                         iselectronic)
                                    for notenum in IRange(numnotes)]
                rankinfos = IndexBy1List([RankInfo(1, 
                                                   voicenum, 
                                                   name,
                                                   rank1voicecomps)])
            except TypeError:
                #print(voicenum, repr(name), ranknum, iselectronic)
                raise
        
        self.rankinfos = IndexBy1List(rankinfos)

        assert voicenum not in VoiceInfo.__cache
        VoiceInfo.__cache[voicenum] = self
    
    def __repr__(self):
        pattern = ("{clsname}({self.voicenum}, {self.name!r}, "
                   "{self.divisionname!r}, {self.numnotes}, "
                   "{self.rankinfos}, "
                   "primaryregoffset={self.primaryregoffset}, "
                   "iselectronic={self.iselectronic}, "
                   "isvirtual={self.isvirtual})")
        return pattern.format(clsname=type(self).__name__, self=self)
    
    def __hash__(self):
        return hash(self.voicenum)

    def lowestpitch(self):
        """return the lowest pitch that can be sounded by this voice"""
        return min([rankinfo.ranknoteinfos[1].soundingpitch 
                    for rankinfo in self.rankinfos
                    if not isinstance(rankinfo.ranknoteinfos[1], 
                                      BlankRanknoteInfo)])
    
    #def countranks(self):
    #    ranknotecounts = [x.countranknotes() for x in self.rankinfos]
    #    from functools import reduce
    #    reduce(lambda a, b: a + b, ranknotecounts)


class VoicenoteState(StateObject):
    """
    VoicenoteState(voiceinfo, notenum [, currentvalue])
    
    a class whose objects are associated with a voicenote and capture the state
    of which registers and how many are requesting to sound that voicenote
    
    voiceinfo (VoiceInfo): the voiceinfo of the voice
    notenum (int): the note number of the voicenote
    currentvalue (set): initial value set of numbers of registers playing the
                        voicenote
    
    Instance attributes:
    
    voiceinfo: (VoiceInfo) the info of the voice whose note's requests are 
                           being counted
    notenum: (int) the note num of the voice whose note's requested are counted
    notereqregsetsig: (Signal of set of registernum)
        a signal containing a set of all the
        numbers of the registers requesting this voicenote
        
        short for note request register set signal

    """

    __slots__ = ["voiceinfo", "notenum", "notereqregsetsig"]
    
    def __init__(self, voiceinfo, notenum, currentvalue=None):
        super().__init__()
        self.voiceinfo = voiceinfo
        self.notenum = notenum
        self.notereqregsetsig = Signal(currentvalue or set())
    
    def __bool__(self):
        return bool(self.notereqregsetsig.val)
    
    def __repr__(self):
        pattern = "<VoicenoteState voice: {} notenum: {} " \
                  "soundedpitches: {} count: {}>"
        return pattern.format(self.voiceinfo.voicenum,
                              self.notenum,
                              list(self.pitchessounded()),
                              self.notereqregsetsig.val)
    
    def reset(self):
        """set the note requests count back to the empty set"""
        super().reset()
        self.notereqregsetsig.next = foo = set()
        assert self.notereqregsetsig.next == foo
    
    def issounding(self):
        """return whether the number of note requests is greater than 0"""
        return len(self.notereqregsetsig.val) > 0
    
    def allsignals(self):
        """return iterable of signals embedded within this object"""
        for x in super().allsignals():
            yield x
        yield self.notereqregsetsig
    
    # this method is private to module organsim.voice
    # it's meant to speed things up by using lists
    def _allsignals(self):
        return tuple(super().allsignals()) + (self.notereqregsetsig,)
            
    def pitchessounded(self):
        """countedvoicenotestate.pitchessounded() -> iterable of pitches"""
        for rank in self.voiceinfo.rankinfos:
            yield rank.ranknoteinfos[self.notenum].soundingpitch


class VoiceState(StateObject):
    """
    VoiceState(voiceinfo)
    
    a count of the number of times (and from which registers) that every note
    in a voice (voicenote) is requested to play
    
    Instance attributes:
    
    voiceinfo: (VoiceInfo) the voice info for this voice note count state
    voicenotestates: (IndexBy1List of VoicenoteState) the note states are 
                                                      indexed by note num
    """
    
    __slots__ = ["voiceinfo", "voicenotestates"]
    
    def __init__(self, voiceinfo):
        super().__init__()
        self.voiceinfo = voiceinfo
        self.voicenotestates = IndexBy1List(VoicenoteState(voiceinfo, notenum) 
                                            for notenum 
                                            in IRange(voiceinfo.numnotes))

    def __repr__(self):
        return "<VoiceState name: %r, soundingstates: %r>" % \
            (self.voiceinfo.name, 
             list(self.soundingstates()))

    def reset(self):
        """return all voice note counts to off"""
        super().reset()
        for notestate in self.voicenotestates:
            notestate.reset()
    
    def isvoiceactive(self):
        """
        return True if there are any sounding pitches in this voice, else False
        """
        return any(notestate.issounding() 
                   for notestate in self.voicenotestates)
    
    def soundingstates(self):
        """iterator over all voicenotestates that are sounding"""
        for voicenotestate in self.voicenotestates:
            if voicenotestate.issounding():
                yield voicenotestate
    
    def soundingnotenums(self):
        """
        return an iterator over the note numbers of all notes that are sounding
        """
        for notenum in IRange(self.voiceinfo.numnotes):
            if self.voicenotestates[notenum].issounding():
                yield notenum
    
    def soundingpitches(self):
        """
        return an iterator of the pitches being sounded by all voice components
        of sounding notes; a pitch may be returned more than once if multiple
        voice components are sounding the same pitch within the voice
        """
        for notenum in self.soundingnotenums():
            for rankinfo in self.voiceinfo.rankinfos:
                try:
                    voicecompinfo = rankinfo.ranknoteinfos[notenum]
                    if voicecompinfo is not None:
                        yield voicecompinfo.soundingpitch
                except IndexError:
                    print('IndexError:')
                    print('\tnotenum:', notenum)
                    print('\trankinfo:', rankinfo)
                    print('\tvoicecompinfo:', voicecompinfo)
                    print()
    
    def allsignals(self):
        """
        obj.allsignals() -> (iterator of Signal)
        
        return iterator of all signals contained within this object
        """
        #for sig in super().allsignals():
        #    yield sig
        #for voicenotestate in self.voicenotestates:
        #    for sig in voicenotestate.allsignals():
        #        yield sig
        return self._allsignals()
    
    # meant to try to speed things up; don't use from outside organsim.voice
    def _allsignals(self):
        mylist = list(super().allsignals())
        for voicenotestate in self.voicenotestates:
            mylist.extend(voicenotestate._allsignals())
        return iter(mylist)


class TotalVoicesState(StateObject):
    """
    TotalVoicesState(voiceinfos)
    
    maps every voice num to a VoiceState
    
    voiceinfos: a dict mapping every voice num to a VoiceInfo object
    
    Instance attributes:
    
    voiceinfos: (dict of VoiceInfo[voicenum]) all voice infos
    voicestates: (dict of VoiceState[voicenum]) all voice states
    """
    
    __slots__ = ["voiceinfos", "voicestates"]
    
    def __init__(self, voiceinfos):
        assert isinstance(voiceinfos, dict)
        self.voiceinfos = voiceinfos
        self.voicestates = {voicenum: VoiceState(voiceinfo)
                            for voicenum, voiceinfo in voiceinfos.items()}
    
    def __repr__(self):
        return "<CountedTotalVoiceState activevoicestates: %r>" % \
            list(self.activevoicestates())
    
    def reset(self):
        """reset all voice states to off"""
        for voicestate in self.voicestates.values():
            voicestate.reset()
    
    def allsignals(self):
        """return all Signal objects contained in this object"""
        for voicestate in self.voicestates.values():
            for voicestatesig in voicestate.allsignals():
                yield voicestatesig
    
    def activevoicestates(self):
        """
        returns an iterator of voices that are active (have notes playing)
        """
        for voicestate in self.voicestates.values():
            if voicestate.isvoiceactive():
                yield voicestate
