try:
    print("starting")
    raise Exception("stuff")
except Exception as x:
    print("got x")
except KeyboardInterrupt as kbi:
    print(":-(")
except:
    print("all other exceptions")
else:
    print("no exception occurred")
finally:
    print("we're done!")

